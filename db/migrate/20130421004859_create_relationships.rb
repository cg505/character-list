class CreateRelationships < ActiveRecord::Migration
  def change
    create_table :relationships do |t|
      t.integer :character_id
      t.integer :relatee_id
      t.string :relation

      t.timestamps
    end
  end
end
