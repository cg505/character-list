class AddChapterToEvents < ActiveRecord::Migration
  def change
    add_column :events, :chapter, :integer
  end
end
