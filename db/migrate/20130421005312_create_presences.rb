class CreatePresences < ActiveRecord::Migration
  def change
    create_table :presences do |t|
      t.integer :character_id
      t.integer :event_id

      t.timestamps
    end
  end
end
