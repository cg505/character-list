class Presence < ActiveRecord::Base
  attr_accessible :character_id, :event_id
  belongs_to :event
  belongs_to :character
end
