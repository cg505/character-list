class Character < ActiveRecord::Base
  attr_accessible :description, :first_appearance, :name, :relatee_tokens
  attr_reader :relatee_tokens
  validates_uniqueness_of :name
  validates_presence_of :name, :first_appearance

  def relatee_tokens=(ids)
    self.relatee_ids = ids.split(",")
    self.character_ids = ids.split(",")
  end

  has_many :relationships, :foreign_key => "character_id"
  has_many :relatees, :through => :relationships
  has_many :alt_relationships, :foreign_key => "relatee_id", :class_name => "Relationship"
  has_many :characters, :through => :alt_relationships
  has_many :presences
  has_many :events, :through => :presences
end
