class Relationship < ActiveRecord::Base
  attr_accessible :character_id, :relatee_id, :relation
  belongs_to :character, :foreign_key => "character_id", :class_name => "Character"
  belongs_to :relatee, :foreign_key => "relatee_id", :class_name => "Character"
end
