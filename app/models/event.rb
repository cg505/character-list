class Event < ActiveRecord::Base
  attr_accessible :description, :name, :chapter, :character_tokens
  attr_reader :character_tokens
  validates_uniqueness_of :name
  validates_presence_of :name, :chapter

  def character_tokens=(ids)
    self.character_ids = ids.split(",")
  end

  has_many :presences
  has_many :characters, :through => :presences
end
